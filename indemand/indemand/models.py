from sqlalchemy import create_engine, Column, Table, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import (
    Integer, SmallInteger, String, Date, DateTime, Float, Boolean, Text, LargeBinary
)
from scrapy.utils.project import get_project_settings

DeclarativeBase = declarative_base()

def db_connect():
    return create_engine(get_project_settings().get("CONNECTION_STRING"))

def create_table(engine):
    DeclarativeBase.metadata.create_all(engine)

class ActualDB(DeclarativeBase):
    __tablename__ = "campaigns"

    url = Column('url', String(250), primary_key=True)
    backers = Column('backers', Integer())
    title = Column('title', Text())
    description = Column('description', Text())
    days_to_go = Column("days_to_go", String(250))
    money_amount = Column ('money_amount', Float())
    state = Column ('state', String(250))
    tags = Column("tags", String(250))
    image = Column('image', String(250) )
    category = Column('category', String(250))
    platform = Column('platform', String(250))
    datetime = Column('datetime', DateTime())
    kickstarter_url = Column("kickstarter_url", String(250))
    indemand_url = Column("indemand_url", String(250))
