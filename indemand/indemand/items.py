
import scrapy


class IndemandItem(scrapy.Item):
    url = scrapy.Field()
    title = scrapy.Field()
    description = scrapy.Field()
    money_amount = scrapy.Field()
    state = scrapy.Field()
    days_to_go = scrapy.Field()
    backers = scrapy.Field()
    category = scrapy.Field()
    image = scrapy.Field()
    tags = scrapy.Field()
    datetime = scrapy.Field()
    old_url = scrapy.Field()
    is_extrenal = scrapy.Field()
