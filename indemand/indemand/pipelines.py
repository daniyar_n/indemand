import sqlalchemy as db

from scrapy.utils.project import get_project_settings
from sqlalchemy.orm import sessionmaker

from elasticsearch import Elasticsearch

from .models import ActualDB, db_connect, create_table

class IndemandPipeline(object):
    def __init__(self):
        engine = db_connect()
        create_table(engine)
        self.Session = sessionmaker(bind=engine)

    def process_item(self, item, spider):
        session = self.Session()
        if item['is_extrenal'] == 'true':
            find = session.query(ActualDB).filter(ActualDB.title == item['title']).filter(ActualDB.platform == 'kickstarter').first()
            if find is not None:
                find.indemand_url = item['url']
                session.commit()
                session.close()
        return item


class IndemandElasticPipeline(object):
    def __init__(self):
        self.es = Elasticsearch(get_project_settings().get("ELASTICSEARCH_SERVER"))

    def process_item(self, item, spider):
        elastic = self.es
        doc_id = 0
        indemand_url = item['url']
        if item['is_extrenal'] == 'true':
            res = elastic.search(index="campaigns", doc_type="items", body={"query": {"match": {"title": item['title']}}})

            for doc in res['hits']['hits']:

                if doc['_source']['platform'] == 'kickstarter' and doc['_source']['title'] == item['title']:
                    doc_id = doc['_id']

                    if doc_id != 0:
                        elastic.update(index='campaigns',doc_type='items',id=doc_id, body={"doc": {"indemand_url": indemand_url}})
