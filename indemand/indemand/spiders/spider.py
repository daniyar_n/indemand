import scrapy
import time
import datetime, time
from scrapy import Spider
from scrapy.http import Request
import json

from scrapy import signals

import sqlalchemy as db

from scrapy.utils.project import get_project_settings

import re

from scrapy.selector import Selector

class Indemand(Spider):
    name = 'indemand_spider'
    allowed_domains = ['www.indiegogo.com']

    def start_requests(self):
        engine = db.create_engine(get_project_settings().get("CONNECTION_STRING"))
        connection = engine.connect()
        metadata = db.MetaData()
        indiegogo = db.Table('campaigns', metadata, autoload=True, autoload_with=engine)
        query = db.select([indiegogo.columns.url]).where(indiegogo.columns.platform == 'indiegogo').where(indiegogo.columns.state == 'indemand')
        request_query = connection.execute(query)
        result_data = request_query.fetchall()
        for item in result_data:
            item = str(item)
            item = item.split("(")[1].split("'")[1]
            yield scrapy.Request(item, self.parse_main, dont_filter=True)


    def parse_main(self, response):

        list = response.xpath("/html/body/script[15]/text()").extract()

        title = response.xpath("//meta[@property='og:title']/@content")[0].extract()

        if list != []:
            text = str(list[0])

            url = response.url

            is_extrenal = re.search('is_external_campaign":(.+?),', text).group(1)

            yield {
                'is_extrenal': is_extrenal,
                'url': url,
                'title': title,
            }
